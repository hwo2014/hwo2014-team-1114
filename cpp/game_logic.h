#ifndef HWO_GAME_LOGIC_H
#define HWO_GAME_LOGIC_H

#include <string>
#include <vector>
#include <map>
#include <functional>
#include <iostream>
#include <jsoncons/json.hpp>

#include <complex>

typedef std::complex<double> Vec2;

struct CollectedData 
{
  void addThrottleSample(double velocity, double throttle, double newVelocity)
  {
    velocities.insert(std::make_pair(velocity, std::make_pair(throttle, newVelocity)));    
    accelerations.insert(std::make_pair(throttle, newVelocity - velocity));
  }

  double guessAcceleration(double velocity, double throttle)
  {
#if 0
    const double throttleScaling = 1000;
    // distance -> sample
    std::multimap<double, double> by_distance;

    for(auto it : velocities) {
      double v = it.first;
      double t = it.second.first;
      double v_ = it.second.second;

      double d = std::pow(std::abs(v - velocity), 2) + std::pow(std::abs(t - throttle)/throttleScaling, 2);
      d = std::sqrt(d);
      by_distance.insert(std::make_pair(d, v_ - v));
    }

    double totalWeight = 0;
    double totalAcceleration = 0;
    int count = 0;
    for(auto it : by_distance) {
      double d = it.first;
      double a = it.second;
      double expo = 2;

      double w = 1.0/std::pow(0.001 + d, expo);
      totalWeight += w;
      totalAcceleration += w*a;
      count++;
      if(count == 5) break;
    }

    double a = totalAcceleration/totalWeight;
    return a;
#else
   // special fit for Finland
   const double normal[] = { 0.019540, -0.195136, 0.980582 };
   return -(normal[0]*velocity + normal[1]*throttle)/normal[2];
#endif
  }

  // 
  double guessThrottle(double velocity, double distance, double endVelocity)
  {
    double pos = 0;
    // can we slow down
    double v = velocity;
    while(pos < distance) {
      double a = guessAcceleration(v, 0.0);
      double v_ = v+a;
      if(v_ <= endVelocity)
        return 1;
      pos += v_;
      v = v_;
    }
    return 0;
  }

  // Throttle -> Acceleration
  std::multimap<double, double> accelerations;

  // Velocity -> (throttle, velocity')
  std::multimap<double, std::pair<double, double>> velocities;
};


struct TrackInfo
{
  jsoncons::json pieces;
  jsoncons::json lanes;
  jsoncons::json startingPoint;

  bool isStraight(int index);
  double pieceLength(int index, int lane);
};

struct Position
{
  Position(int _tick, int index, double dist)
    : tick(_tick), pieceIndex(index), inPieceDistance(dist)
  {}
  
  int tick;
  int pieceIndex;
  double inPieceDistance;
  int startLane, endLane;
  double offsetAngle;
  double velocity;
  double throttle;
  double guessedVelocity;
};

struct State
{
  State() : currentTick(-1) {}
  TrackInfo trackInfo;

  std::string myCarName;

  std::vector<Position> positions;
  int currentTick;
};

class game_logic
{
public:
  typedef std::vector<jsoncons::json> msg_vector;

  game_logic();
  msg_vector react(const jsoncons::json& msg);
  void end();

private:
  typedef std::function<msg_vector(game_logic*, const jsoncons::json&)> action_fun;
  const std::map<std::string, action_fun> action_map;

  msg_vector on_join(const jsoncons::json& data);
  msg_vector on_your_car(const jsoncons::json& data);
  msg_vector on_game_start(const jsoncons::json& data);
  msg_vector on_car_positions(const jsoncons::json& data);
  msg_vector on_crash(const jsoncons::json& data);
  msg_vector on_spawn(const jsoncons::json& data);
  msg_vector on_game_end(const jsoncons::json& data);
  msg_vector on_error(const jsoncons::json& data);

  msg_vector on_game_init(const jsoncons::json& data);
  msg_vector on_lap_finished(const jsoncons::json& data);
  msg_vector on_finish(const jsoncons::json& data);
  msg_vector on_tournament_end(const jsoncons::json& data);

  int tick();
  State m_state;
  CollectedData collectedData;
};

#endif
