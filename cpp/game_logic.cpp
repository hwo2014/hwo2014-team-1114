#include "game_logic.h"
#include "protocol.h"

#include <map>
#include <set>
#include <queue>
#include <fstream>

using namespace hwo_protocol;

bool TrackInfo::isStraight(int index)
{
  return pieces[index % pieces.size()].has_member("length");
}

double TrackInfo::pieceLength(int index, int lane)
{
  jsoncons::json & piece = pieces[index % pieces.size()];
  if(piece.has_member("radius")) {
    double angle = piece["angle"].as<double>() * (M_PI / 180);
    double r = piece["radius"].as<double>();
    r += (angle < 0 ? 1 : -1) * lanes[lane]["distanceFromCenter"].as<double>();
    return std::abs(r * angle);
  } else {
    return piece["length"].as<double>();
  }

}

game_logic::game_logic()
  : action_map
    {
      { "join", &game_logic::on_join },
      { "yourCar", &game_logic::on_your_car },
      { "gameInit", &game_logic::on_game_init},
      { "gameStart", &game_logic::on_game_start },
      { "carPositions", &game_logic::on_car_positions },
      { "lapFinished", &game_logic::on_lap_finished },
      { "crash", &game_logic::on_crash },
      { "spawn", &game_logic::on_spawn },
      { "gameEnd", &game_logic::on_game_end },
      { "error", &game_logic::on_error },
      { "finish", &game_logic::on_finish},
      { "tournamentEnd", &game_logic::on_tournament_end}
    }
{
  srand(time(NULL));
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
  const auto& msg_type = msg["msgType"].as<std::string>();
  const auto& data = msg["data"];

  if(msg.has_member("gameTick")) { 
    int tick = msg.get("gameTick").as<int>();
    m_state.currentTick = tick;
  }
  
  auto action_it = action_map.find(msg_type);
  if (action_it != action_map.end())
  {
    return (action_it->second)(this, data);
  }
  else
  {
    std::cout << "Unknown message type: " << msg_type << std::endl;
    return { make_ping() };
  }
}

void game_logic::end()
{
  std::ofstream of;
  of.open("data_points.txt");
  for(auto it: collectedData.velocities) {
    of << it.first << " " << it.second.first << " " << it.second.second << '\n';
  }
  of.close();
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data)
{
  std::cout << "Joined" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_your_car(const jsoncons::json& data)
{
  m_state.myCarName = data["name"].as_string();
  std::cout << jsoncons::pretty_print(data) << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data)
{
  std::cout << "Race started" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data)
{
  jsoncons::json my_data;
  for(int i=0; i < data.size(); ++i) {
    if(data[i]["id"]["name"].as_string() == m_state.myCarName) {
      my_data = data[i];
    }
  }

  const jsoncons::json & my_position = my_data["piecePosition"];
  int pieceIndex = my_position["pieceIndex"].as<int>();
  double inPieceDistance = my_position["inPieceDistance"].as<double>();
  
  Position p(m_state.currentTick, pieceIndex, inPieceDistance);
  p.offsetAngle = my_data["angle"].as<double>();
  p.startLane = my_position["lane"]["startLaneIndex"].as<int>();
  p.endLane = my_position["lane"]["endLaneIndex"].as<int>();

  double velocity = 0;
  if(m_state.positions.size() > 0) {
    Position & p1 = m_state.positions[m_state.positions.size()-1];
    Position & p2 = p;
    // Calculate real positions & velocity based on track info
    double dist = p2.inPieceDistance - p1.inPieceDistance;
    int pieceCount = m_state.trackInfo.pieces.size();

    int lane = p1.endLane;
    for(int i=p1.pieceIndex; i != p2.pieceIndex; i = (i+1) % pieceCount) {
      dist += m_state.trackInfo.pieceLength(i, lane);
    }
    velocity = dist;    

    if(m_state.trackInfo.isStraight(p1.pieceIndex) && m_state.trackInfo.isStraight(p2.pieceIndex)) {
      double was_velocity = p1.velocity;
      double was_throttle = p1.throttle;

      double guess = collectedData.guessAcceleration(was_velocity, was_throttle);
      double accel = velocity - was_velocity;
      collectedData.addThrottleSample(was_velocity, was_throttle, velocity);

      double guess_ = collectedData.guessAcceleration(was_velocity, was_throttle);

      std::cout << "Velocity guess throttle " << was_throttle << " = " << was_velocity << " -> " << (was_velocity + guess) << ", actual = " << velocity << " (err " << (velocity-(was_velocity+guess)) << "), acc " << guess << "/" << accel << ",  rel err " << (100.0*(guess/accel - 1.0)) << "%, new guess accel " << guess_ << " rel_err = " << (100.0*(guess_/accel-1.0)) << "%, diff = " << (guess_ - accel) << std::endl;
      
    }
    
  }
  p.velocity = velocity;

  double throttle = 0;
  const jsoncons::json & pieces = m_state.trackInfo.pieces;

  int nextCurve = -1;
  int nextStraight = -1;
  for(int i=0; i < pieces.size(); ++i) {
    int idx = pieceIndex + i;
    bool isCurve = pieces[idx % pieces.size()].has_member("radius");
    if(isCurve) {
      if(nextCurve == -1)
        nextCurve = idx;
    } else {
      if(nextStraight == -1)
        nextStraight = idx;
    }

    if(nextCurve != -1 && nextStraight != -1)
      break;
  }

  if(nextCurve == pieceIndex) {
    // Just do some heuristic thing if in a curve
    if(p.inPieceDistance > m_state.trackInfo.pieceLength(p.pieceIndex, p.endLane)-40 && nextStraight == pieceIndex+1)
      throttle = 1.0;
    else if(std::abs(p.offsetAngle) < 5)
      throttle = 0.8;
    else
      throttle = 0.2;
  } else {
    double distance = -inPieceDistance;
    for(int i=pieceIndex; i < nextCurve; ++i) {
      distance += pieces[i % pieces.size()]["length"].as<double>();
    }

    double r = m_state.trackInfo.pieces[nextCurve % m_state.trackInfo.pieces.size()]["radius"].as<double>();
    throttle = collectedData.guessThrottle(velocity, distance, 7.0);

/*
    throttle = 0.2 + 0.8 * (rand()%1001)/1000.0;
    if(distance < 100) {
      if(velocity > 6)
        throttle = 0.0;
       else
        throttle = 0.4;
    }
*/
  }

  p.throttle = throttle;
  m_state.positions.push_back(p);

  return { make_throttle(throttle, m_state.currentTick) };
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data)
{
  std::cout << "Someone crashed" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_spawn(const jsoncons::json& data)
{
  std::cout << "Someone spawned" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data)
{
  std::cout << "Race ended" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data)
{
  std::cout << "Error: " << data.to_string() << std::endl;
  return { make_ping() };
}
  
game_logic::msg_vector game_logic::on_game_init(const jsoncons::json& data)
{
  const jsoncons::json & pieces = data["race"]["track"]["pieces"];
  const jsoncons::json & lanes = data["race"]["track"]["lanes"];
  const jsoncons::json & startingPoint = data["race"]["track"]["startingPoint"];

  m_state.trackInfo.pieces = pieces;
  m_state.trackInfo.lanes = lanes;
  m_state.trackInfo.startingPoint = startingPoint;

  std::cout << jsoncons::pretty_print(data) << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_lap_finished(const jsoncons::json& data)
{
  std::cout << jsoncons::pretty_print(data) << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_finish(const jsoncons::json& data)
{
  std::cout << jsoncons::pretty_print(data) << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_tournament_end(const jsoncons::json& data)
{
  std::cout << jsoncons::pretty_print(data) << std::endl;
  return { make_ping() };
}
